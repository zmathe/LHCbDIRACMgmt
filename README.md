<table>
  <tr>
    <th><strong>Branch</strong></th>
    <th>Master</th>
    <th>Devel</th>
    <th>Coverage QA Branch</th>
  </tr>
  <tr>
    <td><strong>Build status</strong></td>
    <td>
      <a href='https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt/commits/master'>
      <img src='https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt/badges/master/build.svg'></a>
    </td>
    <td>
      <a href='https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt/commits/devel'>
      <img src='https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt/badges/devel/build.svg'></a>
    </td>
    <td>
      <a href="https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt/commits/qa">
      <img alt="coverage report" src="https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt/badges/qa/coverage.svg" /></a>
    </td>
  </tr>
</table>

[Documentation for managing the containers](./containers/README.md)

[Grammar for configuring Mesos](./containers/docs/grammar.md)
