""" API to interact with the lhcb mesos cluster
"""

import logging
import urlparse
from lbmesos.Utils import request_with_raise

#pylint: disable=broad-except


class MarathonAPI(object):
  """ The API object"""

  def __init__(self, marathonMasters):
    """ c'tor

        :param marathonMasters: list of urls (with port) of the masters
    """
    self.marathonMasterList = marathonMasters
    self.marathonLeader = self.findLeader()

  def findLeader(self):
    """ Find the leader in the marathon cluster
        and puts it first in the master list if
        it is already in

        :return: leader's URL
    """

    logging.debug("Trying to find marathon leader")

    response = request_with_raise('GET', self.marathonMasterList, 'v2/leader')

    leader = response.json()['leader']

    if not leader:
      raise ValueError("Marathon cluster seems to have no leader")

    logging.debug("Leader: %s", leader)

    #add the http/https in front
    # find out if http or https
    proto = urlparse.urlparse(self.marathonMasterList[0]).scheme
    leader = '%s://%s'%(proto,leader)
    leaderHost = urlparse.urlparse(leader).netloc

    # if the leader is in the list of marathon hosts, put it in
    # front of the list
    leaderId = None
    for masterId, masterServer in enumerate(self.marathonMasterList):
      if leaderHost in masterServer:
        leaderId = masterId
        leader = masterServer
        break

    # No need to do anything in case leaderId = 0
    if leaderId:
      self.marathonMasterList.insert(
          0, self.marathonMasterList.pop(leaderId))


    return leader

  def getRunningApps(self):
    """ Query Marathon to see the running apps

        :return: list of marathon app json dicts
    """

    res = request_with_raise('GET', self.marathonMasterList, 'v2/apps', timeout = 1)

    apps = res.json()['apps']

    return apps

  def destroyApp(self, appId):
    """ Destroy an application

        :param appId: Id of the application

        :returns: requests.Response
    """

    res = request_with_raise('DELETE', self.marathonMasterList, 'v2/apps/%s' % appId)
    logging.info("Successfuly destroyed app %s", appId)
    return res

  def putConfiguration(self, configList):
    """ Push a configuration for a list of applications

        :param configList: list of marathon app json dicts
        :returns: requests.Response
    """

    res = request_with_raise('PUT', self.marathonMasterList, 'v2/apps', json = configList)
    logging.info("Successfuly sent new config")
    return res

  def replaceRunningConfiguration(self, configList):
    """ Replace the running configuration with the one passed in parameters.
        CAUTION: if an application is running on the cluster,
                                but not passed in the config, it is deleted
                                from the cluster

        :param configList: list of marathon app json dicts
        :returns: requests.Response
    """

    runningApps = self.getRunningApps()
    runningAppIds = set([app['id'] for app in runningApps])
    configAppIds = set([app['id'] for app in configList])

    appIdsToDelete = runningAppIds - configAppIds
    logging.info("Should/Will delete %s",appIdsToDelete)

    response = self.putConfiguration(configList)
    logging.info("Successfuly sent new config")
    return response

    #res = request_with_raise('PUT', self.marathonMasterList, '/v2/apps', json = configList)
    #logging.info("Successfuly sent new config")
    #return res
