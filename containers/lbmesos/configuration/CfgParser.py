""" This containst the CfgParser class to parse the configuration
"""

import urllib2
import json
import os
import copy
import logging


class CfgParser(object):
  """ Parser for the configuration"""

  def __init__(self, configFileName, config = None):
    self.configFileName = configFileName
    # Also works if it is a url
    self.configDir = os.path.dirname(self.configFileName)
    self.config = config if config else json.loads(urllib2.urlopen(configFileName).read())
    logging.debug("Loaded config from %s", self.configFileName)
    logging.debug(self.config)

  def resolvePath(self, path):
    """ Resolve pat with respect to the config files """
    # If it is relative, let's make it relative
    # to the configFileName
    if not os.path.isabs(path):
      path = os.path.join(self.configDir, path)

    return path

  # def _crawlConfig(self, path):
  #   # How nice :-)
  #   # split the path and recursively dig down the json config dictionary
  #   return reduce(lambda d, e: d[e], path.strip('/').split('/'), self.config)
  #
  # def _getValue(self, path, defaultValue = None):
  #   try:
  #     csValue = self._crawlConfig(path)
  #   except KeyError:
  #     if defaultValue is None:
  #       raise KeyError("Could not find option %s" % path)
  #     csValue = defaultValue
  #   return csValue
  #
  # def _getOptionsDict(self, path):
  #   csSection = self._crawlConfig(path)
  #   options = dict((opt, val) for opt, val in csSection.iteritems() if not isinstance(val, dict))
  #   return options
  #
  # def _getOptions(self, path):
  #   csSection = self._crawlConfig(path)
  #   options = [opt for opt, val in csSection.iteritems() if not isinstance(val, dict)]
  #   return options
  #
  # def _getSections(self, path):
  #   csSection = self._crawlConfig(path)
  #   sections = [opt for opt, val in csSection.iteritems() if isinstance(val, dict)]
  #   return sections

  def getMasterURLs(self, framework):
    """ Return the list of URLs of the framework masters

        :returns: list of urls
    """

    logging.debug("getMasterURLs for %s", framework)
    servers = self.config['%sMasterURLs' % framework]
    if isinstance(servers, basestring):
      servers = [servers]

    return servers

  def getTaskLists(self, framework = None):
    """ Returns list of tasks that are declared in the configuration

        :param framework: if specified, returned only the tasks of a given framework

        :returns: list of service ids
    """

    taskConfig = self.getTasksConfiguration(framework)
    return dict((fm, fmTaskConfig.keys()) for fm, fmTaskConfig in taskConfig.iteritems())

  @staticmethod
  def _mergeTaskConfigurationOld(configurationDict):
    """ Given a dictionary, this method recursively go down
        until finding a dictionary that has no sub dictionary.
        At each step, the attributes of a lower level take
        precedences of the higher level.
        The output is a list of tuples (name, attributes dictionary).
        The name is a slash separated concatenation of the level crossed.

        Example:
              "_LHCbDIRAC": {
                  "instances": 1,
                  "template": "toto",
                  "DataManagement": {
                      "instances": 2,
                      "FileCatalog": {
                          "port": 9197
                      },
                      "Bookkeeping": {
                          "instances": 3,
                          "port": 9198
                      }
                  }
              }
        is transformed into
        [(u'_LHCbDIRAC/DataManagement/Bookkeeping/',
          {u'instances': 3, u'port': 9198, u'template': u'toto'}),
         (u'_LHCbDIRAC/DataManagement/FileCatalog/',
          {u'instances': 2, u'port': 9197, u'template': u'toto'})]

        :param configurationDict: directory to go through recursively

        :returns: [(taskName, attributes)]

    """

    logging.debug("Resolving %s", configurationDict)
    subTasks = {}
    config = {}

    for key, value in configurationDict.iteritems():
      logging.debug("Looking at %s", (key))
      if not isinstance(value, dict):
        logging.debug("Simple value")
        config[key] = value
      else:
        logging.debug("Sub task. resolving")
        subTasks[key] = CfgParser._mergeTaskConfiguration(value)

    logging.debug("Done looping")
    if not subTasks:
      logging.debug("No subtasks, just returning config %s", config)

      return [('', config)]

    mergedConfig = []

    logging.debug("Merging sub tasks %s", subTasks)
    for subTask, resolvedSubTasks in subTasks.iteritems():
      logging.debug("Merging %s %s", subTask, resolvedSubTasks)
      for subName, subConfig in resolvedSubTasks:
        configCopy = copy.copy(config)
        configCopy.update(subConfig)
        mergedConfig.append(("%s/%s" % (subTask, subName), configCopy))
    logging.debug("Merged config %s", mergedConfig)

    return mergedConfig

  @staticmethod
  def _mergeTaskConfiguration(configurationDict):
    """ Given a dictionary, this method recursively go down
        until finding a dictionary that has no sub dictionary.
        At each step, the attributes of a lower level take
        precedences of the higher level.
        The output is a list of tuples (name, attributes dictionary).
        The name is a slash separated concatenation of the level crossed.

        Example:
              "_LHCbDIRAC": {
                  "instances": 1,
                  "template": "toto",
                  "DataManagement": {
                      "instances": 2,
                      "FileCatalog": {
                          "port": 9197
                      },
                      "Bookkeeping": {
                          "instances": 3,
                          "port": 9198
                      }
                  }
              }
        is transformed into
        [(u'_LHCbDIRAC/DataManagement/Bookkeeping/',
          {u'instances': 3, u'port': 9198, u'template': u'toto'}),
         (u'_LHCbDIRAC/DataManagement/FileCatalog/',
          {u'instances': 2, u'port': 9197, u'template': u'toto'})]

        :param configurationDict: directory to go through recursively

        :returns: [(taskName, attributes)]

    """

    logging.debug("Resolving %s", configurationDict)
    subTasks = {}
    config = {}

    for key, value in configurationDict.iteritems():
      logging.debug("Looking at %s", (key))
      if not isinstance(value, dict):
        logging.debug("Simple value")
        config[key] = value
      else:
        logging.debug("Sub task. resolving")
        # If the key starts with _, we make it invisible
        if key.startswith('_'):
          name = ''
        else:# With trailing slash
          name = '/' + key

        subTasks[name] = CfgParser._mergeTaskConfiguration(value)

    logging.debug("Done looping")
    if not subTasks:
      logging.debug("No subtasks, just returning config %s", config)

      return {'': config}

    mergedConfig = {}

    logging.debug("Merging sub tasks %s", subTasks)
    for subTask, resolvedSubTasks in subTasks.iteritems():
      logging.debug("Merging %s %s", subTask, resolvedSubTasks)
      for subName, subConfig in resolvedSubTasks.iteritems():
        configCopy = copy.copy(config)
        configCopy.update(subConfig)
        subTaskName = "%s%s" % (subTask, subName) if subName else subTask
        mergedConfig[subTaskName] = configCopy
    logging.debug("Merged config %s", mergedConfig)

    return mergedConfig

  @staticmethod
  def _readTemplateFile(tplFileName):
    """ Read the template file """
    return ''.join(line.strip('\n') for line in urllib2.urlopen(tplFileName).read())

  def _resolveTaskDefinition(self, taskName, taskDefinition):
    """ For the given task definition, returns the full
        definition based on the template attribute.

        'template' must be there, and should point to a templated
        file. The variables inside the template are replaced by the
        content of the directory. Note that in the template,
        the attributes should be all in upper case.
        All variables inside the template have to be defined there.

        A special variable is automaticaly created: SERVICE_ID
        The value is the concatenation of the section, separated by
        slashes (/), with no trailing or ending ones. All in lowercase!!

        Another one, NODE_NAME is the name of the last node in the hierarchy.

        :param taskDefinition: dictionary which must contain a 'template'
    """
    logging.debug("Resolving task definition for %s",taskName)
    # Add default tamplated variables
    taskDefinition['SERVICE_ID'] = taskName.lower()
    taskDefinition['NODE_NAME'] = taskName.split('/')[-1]
    allUpperConfig = dict((k.upper(), v) for k, v in taskDefinition.iteritems())
    tplService = CfgParser._readTemplateFile(self.resolvePath(taskDefinition['template']))
    try:
      configStr = tplService % allUpperConfig
    except:
      logging.error("Incomplete definition for template %s", taskDefinition['template'])
      raise
    configJson = json.loads(configStr)
    return configJson

  def getApplications(self, framework = None):
    """ Return the tasks fully interpreted, ready
        to be given to the framework api

        :param framework: if only interested in a given framework

        :returns: {framork: [fully defined tasks]}
    """

    interpretedTasks = {}
    tasksConfiguration = self.getTasksConfiguration(framework = framework)
    for framework, fmTasksConf in tasksConfiguration.iteritems():
      fmInterpretedTasks = []
      for taskName, taskConfig in fmTasksConf.iteritems():
        try:
          fmInterpretedTasks.append(self._resolveTaskDefinition(taskName, taskConfig))
        except:
          logging.error("Cannot interpret task %s", taskName)
          raise
      interpretedTasks[framework] = fmInterpretedTasks

    return interpretedTasks

  def getTasksConfiguration(self, framework = None):
    """ Returns a json configuration that can be pushed
        directly to the Framework api.

        :param framework: if specified, returns only for a given framework

        :returns: json configuration for framework
    """
    tasks = {}
    if framework:
      tasks[framework] = self._mergeTaskConfiguration(self.config['Tasks'][framework])
    else:
      for framework in self.config['Tasks']:
        tasks[framework] = self._mergeTaskConfiguration(self.config['Tasks'][framework])

    return tasks
