allTasks = {
    "_LHCbDIRAC": {
        "instances": 1,
        "template": "toto",
        "DataManagement": {
            "instances": 2,
            "FileCatalog": {
                "port": 9197
            },
            "Bookkeeping": {
                "instances": 3,
                "boom": "cling"
            },
            "_Sub": {
                "SubSub": {
                    "instances": 3,
                    "port": 9198
                }
            }
        }
    }
}

depth = -1


def Print(s):
  global depth
  print "%s%s" % ('\t' * depth, s)


import logging
logging.getLogger().setLevel(logging.DEBUG)
import copy
import os


def _mergeTaskConfiguration(configurationDict):
  """ Given a dictionary, this method recursively go down
      until finding a dictionary that has no sub dictionary.
      At each step, the attributes of a lower level take
      precedences of the higher level.
      The output is a list of tuples (name, attributes dictionary).
      The name is a slash separated concatenation of the level crossed.

      Example:
            "_LHCbDIRAC": {
                "instances": 1,
                "template": "toto",
                "DataManagement": {
                    "instances": 2,
                    "FileCatalog": {
                        "port": 9197
                    },
                    "Bookkeeping": {
                        "instances": 3,
                        "port": 9198
                    },
                  "Sub": {
                     "SubSub" : {}
                      "instances": 3,
                      "port": 9198
                      }
                  }
                }
            }
      is transformed into
      [(u'_LHCbDIRAC/DataManagement/Bookkeeping/',
        {u'instances': 3, u'port': 9198, u'template': u'toto'}),
       (u'_LHCbDIRAC/DataManagement/FileCatalog/',
        {u'instances': 2, u'port': 9197, u'template': u'toto'})]

      :param configurationDict: directory to go through recursively

      :returns: [(taskName, attributes)]

  """

  logging.debug("Resolving %s", configurationDict)
  subTasks = {}
  config = {}

  for key, value in configurationDict.iteritems():
    logging.debug("Looking at %s", (key))
    if not isinstance(value, dict):
      logging.debug("Simple value")
      config[key] = value
    else:
      logging.debug("Sub task. resolving")
      if key.startswith('_'):
        key = ''
      else:
        key = '/' + key
      subTasks[key] = _mergeTaskConfiguration(value)

  logging.debug("Done looping")
  if not subTasks:
    logging.debug("No subtasks, just returning config %s", config)

    return {'': config}

  mergedConfig = {}

  logging.debug("Merging sub tasks %s", subTasks)
  for subTask, resolvedSubTasks in subTasks.iteritems():
    logging.debug("Merging %s %s", subTask, resolvedSubTasks)
    for subName, subConfig in resolvedSubTasks.iteritems():
      configCopy = copy.copy(config)
      configCopy.update(subConfig)
      print "CHRIS subTask %s subName %s" % (subTask, subName)
      subTaskName = "%s%s" % (subTask, subName) if subName else subTask
      mergedConfig[subTaskName] = configCopy
  logging.debug("Merged config %s", mergedConfig)

  return mergedConfig


print _mergeTaskConfiguration(allTasks)
