# This is the setup.py used to package the deployment script
# and fetch the needed dependecy

import os
from setuptools import setup, find_packages

if os.path.isfile('README.md'):
  long_description = open('README.md').read()
else:
  long_description = '???'

base_dir = os.path.dirname(__file__)
setup(
    name = 'lbmesos',
    description = 'manages the images of LHCbDIRAC used in Mesos',
    version = "0.0.6",
    author = 'Christophe Haen',
    author_email = 'christophe.haen@cern.ch',
    url = 'https://gitlab.cern.ch/lhcb-dirac/LHCbDIRACMgmt',
    license = 'GPLv3',
    long_description = long_description,
    scripts = [
        'scripts/dirac-docker-mgmt.py',
        'scripts/generate-marathon-config.py',
        'scripts/test-marathon-service.py',
        'scripts/push-marathon-configuration.py',
    ],
    packages = find_packages(),
    install_requires = ['docker==2.0.0'], )
