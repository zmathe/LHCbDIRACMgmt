#!/usr/bin/python
"""Take a configuration file as input, and apply the marathon configuration"""

import sys
from lbmesos.marathon.marathonAPI import MarathonAPI
from lbmesos.configuration import CfgManagement

if len(sys.argv) != 2:
  print "Usage: %s <configFile>"%sys.argv[0]
  sys.exit(1)

configFile = sys.argv[1]

cfg = CfgManagement(configFile)
api = MarathonAPI(cfg.getMasterURLs('Marathon'))
apps  = cfg.getApplications()['Marathon']
print api.replaceRunningConfiguration(apps)
