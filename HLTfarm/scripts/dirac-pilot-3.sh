#!/bin/sh
#
# Runs as lhcbprod.
# start the pilot
#

date --utc +"%Y-%m-%d %H:%M:%S %Z vm-pilot Start vm-pilot"
export HOME=/home/lhcbprod
export HOST=`/bin/hostname`

# set the site config
DIRAC_SITE=DIRAC.HLTFarm.lhcb
LHCBDIRAC_SETUP=LHCb-Production
CE_NAME=OnlineCE.lhcb

for i in "$@"
do
case $i in
    --dirac-tmp=*)
    DIRAC_TMP="${i#*=}"
    shift
    ;;
    --dirac-site=*)
    DIRAC_SITE="${i#*=}"
    shift
    ;;
    --lhcb-setup=*)
    LHCBDIRAC_SETUP="${i#*=}"
    shift
    ;;
    --ce-name=*)
    CE_NAME="${i#*=}"
    shift
    ;;
    --vm-uuid=*)
    VM_UUID="${i#*=}"
    shift
    ;;
    --vmtype=*)
    VMTYPE="${i#*=}"
    shift
    ;;
    *)
    # unknown option
    ;;
esac
done

export LHCBDIRAC_TMPDIR=`mktemp -d -p $DIRAC_TMP`
cd $LHCBDIRAC_TMPDIR

# Default if not given explicitly
LHCBDIRAC_SETUP=${LHCBDIRAC_SETUP:-LHCb-Production}

# We might be running from cvmfs or from /var/spool/checkout
export CONTEXTDIR=`readlink -f \`dirname $0\``

export TMPDIR=$LHCBDIRAC_TMPDIR
export EDG_WL_SCRATCH=$TMPDIR

# Needed to find software area
export VO_LHCB_SW_DIR=/cvmfs/lhcb.cern.ch

# Clear it to avoid problems ( be careful if there is more than one agent ! )
rm -rf /tmp/area/*


#run the dirac-pilot script
python /home/lhcbprod/production/Pilot3/dirac-pilot.py \
 --debug \
 --setup=$LHCBDIRAC_SETUP \
 --pilotCFGFile=/home/lhcbprod/production/Pilot3/pilot.json \
 -l LHCb \
 -o lbRunOnly \
 --Name=$CE_NAME \
 --Queue=OnlineQueue \
 --MaxCycles=1 \
 --name=$DIRAC_SITE \
 --cert \
 --certLocation=/home/lhcbprod/production/etc \
 --commandExtensions LHCbPilot

cd /localdisk1/dirac/work
rm -rf $LHCBDIRAC_TMPDIR
