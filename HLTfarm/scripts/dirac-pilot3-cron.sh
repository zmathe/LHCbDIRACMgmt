#!/bin/sh
#
# Runs as lhcbprod. 
# Download latest version of th ePilot3
#

date --utc +"%Y-%m-%d %H:%M:%S %Z vm-pilot Start vm-pilot"
export HOME=/home/lhcbprod

export http_proxy=http://netgw01:8080
export https_proxy=http://netgw01:8080


cd $HOME/production/Pilot3

#
for file in LHCbPilotCommands.py dirac-install.py dirac-pilot.py pilot.json pilotCommands.py pilotTools.py ; do wget --no-check-certificate -q http://lhcb-portal-dirac.cern.ch/pilot/${file} -O ${file}; done

python -m compileall .
