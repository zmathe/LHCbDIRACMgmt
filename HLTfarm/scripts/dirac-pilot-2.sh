#!/bin/sh
#
# Runs as dirac. Sets up to run dirac-pilot.py
#

date --utc +"%Y-%m-%d %H:%M:%S %Z vm-pilot Start vm-pilot"
export HOME=/home/lhcbprod
export HOST=`/bin/hostname`

DIRAC_SITE=DIRAC.HLTFarm.lhcb
LHCBDIRAC_SETUP=LHCb-Production
CE_NAME=OnlineCE.lhcb

for i in "$@"
do
case $i in
    --dirac-tmp=*)
    DIRAC_TMP="${i#*=}"
    shift
    ;;
    --dirac-site=*)
    DIRAC_SITE="${i#*=}"
    shift
    ;;
    --lhcb-setup=*)
    LHCBDIRAC_SETUP="${i#*=}"
    shift
    ;;
    --ce-name=*)
    CE_NAME="${i#*=}"
    shift
    ;;
    --vm-uuid=*)
    VM_UUID="${i#*=}"
    shift
    ;;
    --vmtype=*)
    VMTYPE="${i#*=}"
    shift
    ;;
    *)
    # unknown option
    ;;
esac
done

export LHCBDIRAC_TMPDIR=`mktemp -d -p $DIRAC_TMP`
cd $LHCBDIRAC_TMPDIR

# Default if not given explicitly
LHCBDIRAC_SETUP=${LHCBDIRAC_SETUP:-LHCb-Production}

# JOB_ID is used by when reporting LocalJobID by DIRAC watchdog
#export JOB_ID="$VMTYPE:$VM_UUID"

# We might be running from cvmfs or from /var/spool/checkout
export CONTEXTDIR=`readlink -f \`dirname $0\``

export TMPDIR=$LHCBDIRAC_TMPDIR
export EDG_WL_SCRATCH=$TMPDIR

# Needed to find software area
export VO_LHCB_SW_DIR=/cvmfs/lhcb.cern.ch

# Clear it to avoid problems ( be careful if there is more than one agent ! )
rm -rf /tmp/area/*

# URLs where to get scripts
#DIRAC_INSTALL='https://raw.githubusercontent.com/DIRACGrid/DIRAC/raw/integration/Core/scripts/dirac-install.py'
#DIRAC_PILOT='https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/WorkloadManagementSystem/PilotAgent/dirac-pilot.py'
#DIRAC_PILOT_TOOLS='https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/WorkloadManagementSystem/PilotAgent/pilotTools.py'
#DIRAC_PILOT_COMMANDS='https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/WorkloadManagementSystem/PilotAgent/pilotCommands.py'
#LHCbDIRAC_PILOT_COMMANDS='https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/raw/devel/LHCbDIRAC/WorkloadManagementSystem/PilotAgent/LHCbPilotCommands.py
'
##LHCbDIRAC_PILOT_COMMANDS='http://svn.cern.ch/guest/dirac/LHCbDIRAC/trunk/LHCbDIRAC/WorkloadManagementSystem/PilotAgent/LHCbPilotCommands.py'
#
#echo "Getting DIRAC Pilot 2.0 code from lhcbproject for now..."
#DIRAC_INSTALL='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/dirac-install.py'
#DIRAC_PILOT='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/dirac-pilot.py'
#DIRAC_PILOT_TOOLS='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/pilotTools.py'
#DIRAC_PILOT_COMMANDS='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/pilotCommands.py'

#
##get the necessary scripts
#wget --no-check-certificate -O dirac-install.py $DIRAC_INSTALL
#wget --no-check-certificate -O dirac-pilot.py $DIRAC_PILOT
#wget --no-check-certificate -O pilotTools.py $DIRAC_PILOT_TOOLS
#wget --no-check-certificate -O pilotCommands.py $DIRAC_PILOT_COMMANDS
#wget --no-check-certificate -O LHCbPilotCommands.py $LHCbDIRAC_PILOT_COMMANDS

#run the dirac-pilot script
# -o '/LocalSite/SubmitPool=Test' \
# --pilotCFGLocation=file:///home/lhcbprod/production/etc \
python /home/lhcbprod/production/Pilot/dirac-pilot.py \
 --debug \
 --setup=$LHCBDIRAC_SETUP \
 --project=LHCb \
 --pilotCFGLocation=file:///home/lhcbprod/production/etc \
 -o lbRunOnly \
 --configurationServer=dips://lbvobox100.cern.ch:9135/Configuration/Server,dips://lbvobox101.cern.ch:9135/Configuration/Server,dips://lbvobox102.ce
rn.ch:9135/Configuration/Server,dips://lbvobox103.cern.ch:9135/Configuration/Server \
 --Name=$CE_NAME \
 --Queue=OnlineQueue \
 --MaxCycles=1 \
 --name=$DIRAC_SITE \
 --cert \
 --certLocation=/home/lhcbprod/production/etc \
 --commandExtensions LHCbPilot \
 --commands LHCbGetPilotVersion,CheckWorkerNode,LHCbInstallDIRAC,LHCbConfigureBasics,CheckCECapabilities,CheckWNCapabilities,LHCbConfigureSite,LHCb
ConfigureArchitecture,LHCbConfigureCPURequirements,LaunchAgent

cd /localdisk1/dirac/work
rm -rf $LHCBDIRAC_TMPDIR

#rm -rf *
